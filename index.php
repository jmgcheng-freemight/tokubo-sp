﻿<?php 
	include 'header.php';
?>

	<main class="">

		<div class="l-content">
			<div class="l-gutter">

				<div class="page-portrait">
					<img src="img/img-1.jpg" />
				</div>

			</div>
		</div>		

		<div class="bgcontent bgcontent-sweets">

			<div class="bgcontent-sweets-slider">
				<div>
					<img src="img/sweet-bg-3.png" />
				</div>
				<div>
					<img src="img/sweet-bg-4.png" />
				</div>
			</div>

			<div class="l-content">
				<div class="l-gutter">

					<div class="sidecontent">

						<div class="l-table width-full">
							<div class="l-table-cell">
								<div class="sidecontent-left">
								</div>
							</div>
							<div class="l-table-cell">
								<div class="sidecontent-right">
									<img src="img/top-sidecontent-1.jpg"  />
									<div class="">
										<a href="#" class="button button-yellow width-full">
											<i class="icon-cart"></i> &nbsp; とくぼうを購入
										</a>
									</div>
									<img src="img/top-sidecontent-2.jpg"  />
									<div class="">
										<a href="#" class="button button-yellow width-full">
											<i class="icon-cart"></i> &nbsp; とくぼうを購入
										</a>
									</div>
									<img src="img/top-sidecontent-3.jpg"  />
									<div class="">
										<a href="#" class="button button-yellow width-full">
											<i class="icon-cart"></i> &nbsp; とくぼうを購入
										</a>
									</div>
									<img src="img/top-sidecontent-4.jpg"  />
									<div class="">
										<a href="#" class="button button-yellow width-full">
											<i class="icon-cart"></i> &nbsp; とくぼうを購入
										</a>
									</div>
									<img src="img/top-sidecontent-5.jpg"  />
									<img src="img/top-sidecontent-6.jpg"  />
								</div>
							</div>
						</div>

					</div>


					<div class="pagecontrol">

						<a href="#" class="button button-yellow width-full">
							<i class="icon-cart"></i> &nbsp; とくぼうを購入
						</a>
						<a href="#" class="button button-red width-full">
							戻る
						</a>

					</div>



				</div>
			</div>



		</div>

	</main>


<?php 
	include 'footer.php';
?>
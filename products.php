﻿<?php 
	include 'header.php';
?>

	<main class="">

		<div class="l-content">
			<div class="l-gutter">

				<div class="page-portrait">
					<img src="img/img-1.jpg" />
				</div>


				<div class="product-cards">
					<ul>
						<li>
							<a class="product-cards-anc" href="product.php?id=1">
								<div class="product-cards-portrait">
									<div class="product-cards-portrait-inner">
										<img src="img/img-2.jpg" />
									</div>
								</div>
								<div class="product-cards-detail">
									<div class="l-table width-full">
										<div class="l-table-cell">
											<h4 class="product-cards-name">とくぼう チーズ10本</h4>
										</div>
										<div class="l-table-cell align-right">
											<span class="product-cards-price">1,600円</span>
										</div>	
									</div>
								</div>
							</a>
						</li>
						<li>
							<a class="product-cards-anc" href="product.php?id=2">
								<div class="product-cards-portrait">
									<div class="product-cards-portrait-inner">
										<img src="img/img-3.jpg" />
									</div>
								</div>
								<div class="product-cards-detail">
									<div class="l-table width-full">
										<div class="l-table-cell">
											<h4 class="product-cards-name">とくぼう あんこ10本</h4>
										</div>
										<div class="l-table-cell align-right">
											<span class="product-cards-price">1,600円</span>
										</div>	
									</div>
								</div>
							</a>
						</li>
						<li>
							<a class="product-cards-anc" href="product.php?id=3">
								<div class="product-cards-portrait">
									<div class="product-cards-portrait-inner">
										<img src="img/img-4.jpg" />
									</div>
								</div>
								<div class="product-cards-detail">
									<div class="l-table width-full">
										<div class="l-table-cell">
											<h4 class="product-cards-name">とくぼう チーズ5本　あんこ5本</h4>
										</div>
										<div class="l-table-cell align-right">
											<span class="product-cards-price">1,600円</span>
										</div>	
									</div>
								</div>
							</a>
						</li>
					</ul>
				</div>



				<div class="pagecontrol">
					<a href="#" class="button button-yellow width-full button-tallness-medium">
						買い物かごを見る
					</a>
					<a href="#" class="button button-red width-full button-tallness-medium">
						戻る
					</a>
				</div>

			</div>
		</div>		


	</main>


<?php 
	include 'footer.php';
?>
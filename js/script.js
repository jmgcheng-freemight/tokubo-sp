$(document).ready(function(){

	/*
		lazy-load
	*/
	var el = $("<div>");
	$.support.transform  = typeof el.css("transform") === "string";
    $.support.transition = typeof el.css("transitionProperty") === "string";
    if($.support.transform && $.support.transition) {
		$(window).on("load scroll", function(){
			var i_screen = $(window).scrollTop() + $(window).height();
			$('.lazy-load').each(function(index, e){
				if( i_screen > ( $(this).offset().top + $(this).height() ) && !$(this).hasClass('lazy-load-show')){
					$(this).addClass('lazy-load-show');
				}
			});
		});
    }
    else
    {
    	$('.lazy-load').addClass('lazy-load-show');
    }


    /*
    $(".bgcontent.bgcontent-sweets").vegas({
	    slides: [
	        { src: "img/sweet-bg-3.png" },
	        { src: "img/sweet-bg-4.png" }
	    ]
	});
    */
    $(".bgcontent-sweets-slider").slick({
		dots: false,
		infinite: true,
		/*centerMode: true,
		variableWidth: true,
		autoplaySpeed: 100,
		*/
		autoplay: true,
		fade: true,
		cssEase: 'linear',
  		adaptiveHeight: true,
  		arrows: false
	});

	
    /**/
    $('.nav-trigger-button').on('click', function(e){
    	e.preventDefault();
    	e.stopPropagation();
    	$(this).siblings('.nav-trigger-menu').toggleClass('show');
    });


    /**/
    $('main, footer').click(function() {
		o_menus = $('.nav-trigger-menu');
		if( o_menus.hasClass('show') )
		{ o_menus.removeClass('show'); }
	});


	/**/
	$(".slider-slick").slick({
		dots: false,
		infinite: true,
		/*centerMode: true,
		variableWidth: true,*/
		autoplay: true,
		fade: true,
		cssEase: 'linear',
  		adaptiveHeight: true,
  		arrows: true
	});
	updateSlickListHeight = function() {
		try {
			i_height = $('.slick-list img').height();
			$('.slick-list').height(i_height);
		}
		catch(e) {}
	}
	updateSlickListHeight();
	$( window ).resize(function() {
		updateSlickListHeight();		
	});

	




});

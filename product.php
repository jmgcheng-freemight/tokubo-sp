﻿<?php 
	include 'header.php';

		/* these should be in db.. */
		$a_product_details = array();
		if( isset($_GET['id']) && !empty($_GET['id']) ) {
			$a_product_details['i_id'] = $_GET['id'];
		}
		else {
			$a_product_details['i_id'] = 1;	
		}

		if( $a_product_details['i_id'] == 1 ) {
			$a_product_details['s_product_name'] = 'とくぼう チーズ10本';
			$a_product_details['a_product_gallery'] = array();
			array_push( $a_product_details['a_product_gallery'], "img/img-2.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-5.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-7.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-1.jpg" );
		}
		elseif ( $a_product_details['i_id'] == 2 ) {
			$a_product_details['s_product_name'] = 'とくぼう あんこ10本';
			$a_product_details['a_product_gallery'] = array();
			array_push( $a_product_details['a_product_gallery'], "img/img-3.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-6.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-8.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-1.jpg" );
		}
		elseif ( $a_product_details['i_id'] == 3 ) {
			$a_product_details['s_product_name'] = 'とくぼう チーズ5本 あんこ5本';
			$a_product_details['a_product_gallery'] = array();
			array_push( $a_product_details['a_product_gallery'], "img/img-4.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-5.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-6.jpg" );
			array_push( $a_product_details['a_product_gallery'], "img/img-1.jpg" );
		}
		else {
			header("Location: products.php");
			die();
		}

?>

	<main class="">

		<div class="l-content">
			<div class="l-gutter">

				<div class="breadcrumb">
					<a href="index.php">TOP</a> > <a href="#"><?php if( isset($a_product_details['s_product_name']) && !empty($a_product_details['s_product_name']) ) { echo $a_product_details['s_product_name']; } ?></a>
				</div>

				<div class="product-brochure">

					<?php 
						if( isset($a_product_details['a_product_gallery']) && !empty($a_product_details['a_product_gallery']) ):
					?>
					<div class="slider slider-products slider-slick">
						<?php 
							foreach( $a_product_details['a_product_gallery'] AS $s_product_gallery ):
						?>
						<div>
							<img src="<?php echo $s_product_gallery; ?>" />
						</div>
						<?php 
							endforeach;
						?>
					</div>
					<?php 
						endif;
					?>

					<div class="product-brochure-desc">
						<h2>
							<?php if( isset($a_product_details['s_product_name']) && !empty($a_product_details['s_product_name']) ) { echo $a_product_details['s_product_name']; } ?>
							<span>
								価格 ¥1,600 （税込）
							</span>
						</h2>
					</div>

					<div class="pagecontrol align-center">
						<a href="#" class="button button-yellow width-full button-tallness-medium ">
							<!-- <span class="button-ico button-ico-left"></span> -->
							<i class="icon-cart"></i> &nbsp;&nbsp; 買い物かごに入れる
						</a>
					</div>						

					<div class="product-brochure-misccontent">

						<img src="img/prod-misc-desc-1.jpg" />
						<img src="img/prod-misc-desc-2.jpg" />
						<img src="img/prod-misc-desc-3.jpg" />
						<img src="img/prod-misc-desc-4.jpg" />
						<img src="img/prod-misc-desc-5.jpg" />
						<img src="img/prod-misc-desc-6.jpg" />

					</div>

					<div class="pagecontrol align-center">
						<a href="#" class="button button-yellow width-full button-tallness-medium ">
							<!-- <span class="button-ico button-ico-left"></span> -->
							<i class="icon-cart"></i> &nbsp;&nbsp; 買い物かごに入れる
						</a>

						<a href="#" class="button button-red width-full button-tallness-medium ">
							戻る
						</a>
					</div>	

				</div>







			</div>
		</div>		


	</main>


<?php 
	include 'footer.php';
?>